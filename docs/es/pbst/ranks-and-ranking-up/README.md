# Puntos y promociones de rango
## Abreviaciones
 * ST (auto entrenamiento)
 * PBST (Equipo de Seguridad de Pinewood Builders)

## Cómo conseguir puntos
Los puntos se pueden conseguir de varias maneras, puedes seguir un [ST](#abbreviations). Atender a un entrenamiento, o participar en eventos como asaltos, y otras cosas. Dependiendo de lo que hagas, conseguirás puntos en base a lo que hagas. Estando en un asalto (en el equipo del [PBST](#abbreviations)).

Los entrenamientos también son una opción, los entrenamientos se anuncian en nuestro muro de grupo de Roblox. Los entrenamientos pueden ser organizados por:
 * Entrenadores
 * Tier 4

Cualquiera puede ser un asistente en un entrenamiento, pero el Anfitrión/Entrenador elige quien va a ser un asistente, pedir ser asistente no ayuda en este caso. Esto sólo reducirá tus posibilidades de convertirte en un asistente, puedes encontrar más información sobre algunas de las reglas [aquí](../handbook/#training).

Como última opción, siempre puedes seguir un [ST](#abbreviations). Estos entrenamientos se realizan específicamente para que la gente consiga puntos de una manera diferente y para practicar sus habilidades en una actividad específica. Más información se dará [aquí](#self-trainings)

## Subir de rango

Subir de rango requiere puntos, que se pueden conseguir en entrenamientos, [ST](#abbreviations) y patrullas. Si obtienes la cantidad de puntos requerida para el siguiente rango, serás promovido. La cantidad necesaria de puntos para cualquier rango se puede encontrar en el spawn en [Centro de Actividad de PBST](https://www.roblox.com/games/1564828419/PBST-Activity-Center), ~~aunque puedes ignorar la parte de las evaluaciones, estas sólo están en su lugar para convertirse en Nivel 4~~. Ahora esto ha cambiado. Por favor, lee el mensaje a continuación...

:::danger ¡Recuerda! 
Cuando eres `Tier1 o superior`, puedes ser sancionado más duramente por errores que cometas, se supone que eres un modelo a seguir para todos los cadetes y visitantes en las **Instalaciones de Pinewood**. Esto no tiene exepción (a menos que se indique lo contrario, como la regla uniforme de T4) 
:::


## Evaluaciones de nivel
Una vez que alcances 100 puntos, deberás participar en una evaluación de nivel si deseas obtener el rango de **Tier 1**. Como con los entrenamientos normales, estos se pueden encontrar en [el horario](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility). Serán organizados regularmente para acomodar todas las zonas horarias.

Si fallas la evaluación, pero tuviste éxito en completar una sección de la evaluación, no tienes que rehacer lo que has completado.

:::danger NO SUPERES LOS 150 PUNTOS 
Si un cadete excede 150 puntos antes de aprobar la evaluación, sus puntos serán reducidos a 150 una vez que los pase. Esto es para evitar que los cadetes que no han pasado su evaluación consigan más puntos y lleguen a tener un gran número de puntos para el momento en que pasen su evaluación. 
:::

Hay un servidor especializado para evaluaciones de nivel, utiliza el comando `!pbstevalserver` para llegar allí. Esto solo funciona si tienes 100 puntos o más.

**Una evaluación de nivel consiste en 2 partes**:
  1. Un cuestionario y una prueba de las habilidades de patrulla.
  2. Las normas de entrenamiento se aplicarán en gran medida en este examen, y no seguirlas se traducirán en un fracaso inmediato.

Durante el cuestionario, recibirás 8 preguntas sobre varios temas, incluyendo este manual y las reglas de entrenamiento. Las preguntas variarán de dificultad, algunas son fáciles pero otras requieren más reflexión. Necesitas contestar correctamente al menos 5/8 para aprobar. La respuesta debe hacerse en privado a través de un mensaje privado en el chat (usando /w) o de un sistema de mensajes privados enviado por el anfitrión.

Durante la prueba de patrulla recibirás equipamientos **Tier 1** y serás probado en habilidades de combate, trabajo en equipo y seguir las reglas del manual correctamente. Un número de Tier 3 trabajarán en tu contra de en esta parte, ya que tratarán de derretir o congelar el núcleo.

Una vez que hayas pasado la evaluación de nivel, serás ascendido a **Tier 1**. Con tu nuevo rango, recibirás acceso a un nuevo set de armamento en las instalaciones de Pinewood: una porra más potente, un escudo antidisturbios, un táser y una pistola del PBST. Estas armas se pueden encontrar en la sala de seguridad de las instalaciones de Pinewood. Los cadetes no están permitidos en las salas de armamento de los Tiers.

## ¿Cuándo se registrarán los puntos?
Esto depende de si el instructor está disponible y tiene el tiempo para registrar los puntos. Y no podemos decir esto lo suficiente, pero **no pidáis a los entrenadores que los puntos sean registrados**. Esto sólo será molesto para los entrenadores que registran los puntos en cuestión. Y generalmente es spamming si muchos dicen **SEÑOR SEÑOR PUNTOS PORFAVOR REGISTRE LOS PUNTOS**. Los entrenadores pueden registrar puntos, lo que significa que también pueden **RESTARLOS**. Como advertencia que puedo darte, **NO** pidas que se registren puntos, a menos que un entrenador le diga que le pidas que se registren (Toma la imagen como ejemplo)

<img class="no-medium-zoom zooming" src="https://i.imgur.com/yNHkmzm.png" alt="demo" width="1228" />

## Auto Entrenamientos
Cuando te autoentrenas, puedes conseguir puntos sin estar en un entrenamiento, los puntos que consigas pueden cambiar dependiendo de tu rango. Si progresas en Tiers, también consigues menos puntos en los auto entrenamientos. Esto se debe al hecho de que deberías poder hacer un nivel más alto si eres un rango más alto.

Una imagen creada por **vgoodedward** muestra esto de una buena manera:
<img class="no-medium-zoom zooming" src="https://i.imgur.com/ho3u0a5.png" alt="demo" width="1228" />

## Entrenamientos de práctica

Los entrenamientos de practicas, también conocidos como PT (Practice Training en ingles) pueden ser organizados por cualquier miembro del PBST en cualquier instalación (preferiblemente en el PBSTAC o PBSTTF). Estos entrenamientos no son oficiales y no conseguirás puntos de ninguna manera.

Los PT normales normalmente consisten en unos pocos asistentes con un Tier o un cadete de pie en el podio/pad como anfitrión. Antes de que un PT pueda comenzar la persona que organiza el PT, debe aclarar que no es un entrenamiento oficial y explicar que no conseguirás ningún punto por ello. No se permite organizar un PT 30 minutos antes de un entrenamiento oficial (o 15 minutos con el permiso del anfitrión del entrenamiento oficial).

Esto significa que un PT debe terminar 30-15 minutos antes de que comience un entrenamiento oficial

Si una persona afirma ser un verdadero Entrenador o Tier 4, toma una captura de pantalla e informa del usuario a un alto rango. 


