module.exports = [{
    text: '首頁',
    link: '/zh/'
},
{
    text: 'PBST手冊',
    link: '/zh/pbst/'
},
{
    text: 'Pinewood',
    items: [{
            text: 'PET-Handbook',
            link: 'https://pet.pinewood-builders.com'
        },
        {
            text: 'TMS-Handbook',
            link: 'https://tms.pinewood-builders.com'
        }
    ]
}
]